//
//  ViewController.swift
//  Hustle-Mode
//
//  Created by Cameron Tredoux on 7/23/17.
//  Copyright © 2017 Cameron Tredoux. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var darkBlueBG: UIImageView!
    @IBOutlet weak var powerButton: UIButton!
    @IBOutlet weak var cloudsView: UIView!
    @IBOutlet weak var rocketAndStream: UIImageView!
    @IBOutlet weak var hustleModeLabel: UILabel!
    @IBOutlet weak var onLabel: UILabel!
    
    var player: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = Bundle.main.path(forResource: "hustle-on", ofType: "wav")!
        let url = URL(fileURLWithPath: path)
        do {
            
            player = try AVAudioPlayer(contentsOf: url)
            player.prepareToPlay()
            
        } catch let error as NSError {
            
            print(error.description)
            
        }
        
    }

    @IBAction func powerButtonPress(_ sender: Any) {
    
        darkBlueBG.isHidden = true
        powerButton.isHidden = true
        cloudsView.isHidden = false
        
        player.play()
        
        UIView.animate(withDuration: 2.0, animations: {
            
            self.rocketAndStream.frame = CGRect(x: 0, y: 225, width: 375, height: 250)
            
        }) { (finished) in
            
            self.hustleModeLabel.isHidden = false
            self.onLabel.isHidden = false
            
        }
    
    }
    
}

